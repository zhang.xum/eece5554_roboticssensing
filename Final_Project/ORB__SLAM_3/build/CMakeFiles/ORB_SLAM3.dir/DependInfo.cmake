# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/mingee/ORB_SLAM3/src/Atlas.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/Atlas.cc.o"
  "/home/mingee/ORB_SLAM3/src/CameraModels/KannalaBrandt8.cpp" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/CameraModels/KannalaBrandt8.cpp.o"
  "/home/mingee/ORB_SLAM3/src/CameraModels/Pinhole.cpp" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/CameraModels/Pinhole.cpp.o"
  "/home/mingee/ORB_SLAM3/src/Converter.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/Converter.cc.o"
  "/home/mingee/ORB_SLAM3/src/Frame.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/Frame.cc.o"
  "/home/mingee/ORB_SLAM3/src/FrameDrawer.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/FrameDrawer.cc.o"
  "/home/mingee/ORB_SLAM3/src/G2oTypes.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/G2oTypes.cc.o"
  "/home/mingee/ORB_SLAM3/src/ImuTypes.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/ImuTypes.cc.o"
  "/home/mingee/ORB_SLAM3/src/Initializer.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/Initializer.cc.o"
  "/home/mingee/ORB_SLAM3/src/KeyFrame.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/KeyFrame.cc.o"
  "/home/mingee/ORB_SLAM3/src/KeyFrameDatabase.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/KeyFrameDatabase.cc.o"
  "/home/mingee/ORB_SLAM3/src/LocalMapping.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/LocalMapping.cc.o"
  "/home/mingee/ORB_SLAM3/src/LoopClosing.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/LoopClosing.cc.o"
  "/home/mingee/ORB_SLAM3/src/MLPnPsolver.cpp" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/MLPnPsolver.cpp.o"
  "/home/mingee/ORB_SLAM3/src/Map.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/Map.cc.o"
  "/home/mingee/ORB_SLAM3/src/MapDrawer.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/MapDrawer.cc.o"
  "/home/mingee/ORB_SLAM3/src/MapPoint.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/MapPoint.cc.o"
  "/home/mingee/ORB_SLAM3/src/ORBextractor.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/ORBextractor.cc.o"
  "/home/mingee/ORB_SLAM3/src/ORBmatcher.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/ORBmatcher.cc.o"
  "/home/mingee/ORB_SLAM3/src/OptimizableTypes.cpp" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/OptimizableTypes.cpp.o"
  "/home/mingee/ORB_SLAM3/src/Optimizer.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/Optimizer.cc.o"
  "/home/mingee/ORB_SLAM3/src/PnPsolver.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/PnPsolver.cc.o"
  "/home/mingee/ORB_SLAM3/src/Sim3Solver.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/Sim3Solver.cc.o"
  "/home/mingee/ORB_SLAM3/src/System.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/System.cc.o"
  "/home/mingee/ORB_SLAM3/src/Tracking.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/Tracking.cc.o"
  "/home/mingee/ORB_SLAM3/src/TwoViewReconstruction.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/TwoViewReconstruction.cc.o"
  "/home/mingee/ORB_SLAM3/src/Viewer.cc" "/home/mingee/ORB_SLAM3/build/CMakeFiles/ORB_SLAM3.dir/src/Viewer.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "COMPILEDWITHC11"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../"
  "../include"
  "../include/CameraModels"
  "/usr/include/eigen3"
  "../Pangolin/include"
  "../Pangolin/build/src/include"
  "/usr/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
