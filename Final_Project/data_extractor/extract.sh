#!/bin/bash

BASEDIR=$(dirname "$0")
BAG="${BASEDIR}"/morning_stereo_rgb_ir_lidar_gps.bag
CONFIG="${BASEDIR}"/config/config.yaml

# Extract info
python3 "${BASEDIR}"/data_extractor.py

# Extract images
python "${BASEDIR}"/bag2image.py -i "${BAG}" -o "${BASEDIR}" -c "${CONFIG}"
