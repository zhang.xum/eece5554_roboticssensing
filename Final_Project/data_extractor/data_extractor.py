#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bagpy import bagreader

b = bagreader('morning_stereo_rgb_ir_lidar_gps.bag')

topic_table = b.topic_table
topic_table.to_csv("topic_table.csv")
print(topic_table)

topics_extracted = ['/flir_boson/camera_info',
                    '/imu/imu',
                    '/tf',
                    '/tf_static',
                    '/vehicle/gps/fix']

for topic in topics_extracted:
    print("Extracting %s..." % topic)
    b.message_by_topic(topic)

print("Extracting info one.")
