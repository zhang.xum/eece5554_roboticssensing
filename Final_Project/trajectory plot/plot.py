#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import folium
import utm

plt.style.use('ggplot')

# Pre-processes data.
df_gps = pd.read_csv('vehicle_gps_fix.csv')
latitudes_gps = df_gps['latitude']
longitudes_gps = df_gps['longitude']
gps_time = df_gps['Time'] - df_gps['Time'][0]

easting_gps, northing_gps, zone_number, zone_letter = utm.from_latlon(np.array(latitudes_gps), np.array(longitudes_gps))
easting_base = easting_gps[0]
northing_base = northing_gps[0]
easting_gps = easting_gps - easting_base
northing_gps = northing_gps - northing_base

df_slam = pd.read_csv('slam_key_frame_trajectory.csv')
x = df_slam['x']
z = df_slam['z']
slam_time = df_slam['timestamp']

# Scales and corrects the SLAM data.
theta = np.deg2rad(22.5)
R = np.array([[np.cos(theta), - 0.5 * np.sin(theta)], [np.sin(theta), np.cos(theta)]])
slam_corrected = []

for i in range(df_slam.shape[0]):
    p = np.array([[x[i]], [z[i]]])
    slam_corrected.append(R.dot(p))

slam_corrected = np.squeeze(slam_corrected)
easting_slam = slam_corrected[:, 0] * 106.4 + 21.5
northing_slam = slam_corrected[:, 1] * 77.1 - 129

# Plots the comparison of the ORB-SLAM3 and GPS data.
plt.figure(figsize=(12, 8))
plt.xlabel('UTM Easting (m)')
plt.ylabel('UTM Northing (m)')
plt.plot(easting_gps, northing_gps, c='r', label='GPS')
plt.plot(easting_slam, northing_slam, c='b', label='ORB-SLAM3')
plt.title('UTM Coordinates: ORB-SLAM3 VS GPS')
plt.legend()
plt.savefig('slam_gps_utm.png', dpi=600, bbox_inches='tight')

# Plots the errors between the ORB-SLAM3 and GPS data.
t_matching = []
errors = []

for i_gps, t_gps in enumerate(gps_time):
    diff = [abs(t_slam - t_gps) for t_slam in slam_time]

    if min(diff) < 1:
        i_slam = np.argmin(diff)
        err = np.linalg.norm([easting_slam[i_slam] - easting_gps[i_gps],
                              northing_slam[i_slam] - northing_gps[i_gps]])
        errors.append(err)
        t_matching.append(t_gps)

plt.figure(figsize=(12, 8))
plt.xlabel('Time (s)')
plt.ylabel('Error (m)')
plt.plot(t_matching, errors, c='orange')
plt.title('Errors: ORB-SLAM3 VS GPS')
plt.savefig('slam_gps_errors.png', dpi=600, bbox_inches='tight')

# Plots the routes on the real map
location_gps = []
for i in range(df_gps.shape[0]):
    location_gps.append([latitudes_gps[i], longitudes_gps[i]])

latitudes_slam, longitudes_slam = utm.to_latlon(easting_slam + easting_base, northing_slam + northing_base,
                                                zone_number, zone_letter)
location_slam = []
for i in range(df_slam.shape[0]):
    location_slam.append([latitudes_slam[i], longitudes_slam[i]])

m = folium.Map(location=[latitudes_gps.mean(), longitudes_gps.mean()], zoom_start=17)
tile = folium.TileLayer(
    tiles='https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
    attr='Esri',
    name='Esri Satellite',
    overlay=False,
    control=True
).add_to(m)

folium.PolyLine(location_gps,
                color='red',
                weight=5,
                opacity=0.8).add_to(m)
folium.PolyLine(location_slam,
                color='blue',
                weight=5,
                opacity=0.8).add_to(m)

folium.Marker(
    location=location_gps[0],
    icon=folium.Icon(color="red")
).add_to(m)
folium.Marker(
    location=location_slam[0],
    icon=folium.Icon(color="blue")
).add_to(m)

m.save("slam_gps_map.html")
