import numpy as np
import cv2 as cv
import glob

img = cv.imread('sample.jpg')
w = 640
h = 512
#mtx = np.matrix([5.24888150200348832e+02, 0, 3.25596989785447420e+02])
mtx = np.matrix([[5.24888150200348832e+02, 0, 3.25596989785447420e+02], 
    [0, 5.21776791343664968e+02, 2.42392342491041603e+02],
    [0, 0, 1]])

#mtx = cv.UMat(mtx,all)

dist = np.matrix([-4.70302508060718438e-01, 3.01057860458473880e-01, 4.68835914496582538e-03, 1.65573977268025185e-03 ])
newcameramtx, roi = cv.getOptimalNewCameraMatrix(mtx, dist, (w,h), 1, (w,h))
# undistort

# This method uses the function and the particular ROI
dst = cv.undistort(img, mtx, dist, None, newcameramtx)

# crop the image
x,y,w,h = roi
dst = dst[y:y+h, x:x+w]
cv.imwrite('calibresult.png',dst)

